import numpy as np
import soundfile as sf

sample_rate = 44100
frequency = 440.0
duration = 20

# Time axis
t = np.linspace(0, duration, int(sample_rate * duration), False)

# Generate a sine wave at 440 Hz
sine_wave = 0.5 * np.sin(2 * np.pi * frequency * t)

# Save the sine wave as a WAV file
sf.write('440hz_sine_wave.wav', sine_wave, sample_rate)
