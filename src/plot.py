import numpy as np
import matplotlib.pyplot as plt

sample_rate = 44100
carrier_frequency = 440.0
initial_modulating_frequency = 0.0
final_modulating_frequency = 20.0
duration = 5

def generate_sweeping_modulation(t):
    carrier_wave = 0.5 * np.sin(2 * np.pi * carrier_frequency * t)
    modulating_frequency_t = np.linspace(initial_modulating_frequency, final_modulating_frequency, len(t))
    modulating_wave = np.sin(2 * np.pi * modulating_frequency_t * t)
    return (1 + 0.5 * modulating_wave) * carrier_wave

def generate_binary_modulation(t):
    carrier_wave = 0.5 * np.sin(2 * np.pi * carrier_frequency * t)
    modulating_frequency_t = np.linspace(initial_modulating_frequency, final_modulating_frequency, len(t))
    modulating_wave = np.sign(np.sin(2 * np.pi * modulating_frequency_t * t))
    return (0.5 + 0.5 * modulating_wave) * carrier_wave

def main():
    # Time axis
    t = np.linspace(0, duration, int(sample_rate * duration), False)
    
    sweeping_modulated_wave = generate_sweeping_modulation(t)
    binary_modulated_wave = generate_binary_modulation(t)

    # Plot the output of both functions
    plt.figure(figsize=(10, 6))

    plt.subplot(2, 1, 1)
    plt.plot(t, sweeping_modulated_wave)
    plt.title('Sweeping Modulation')
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')

    plt.subplot(2, 1, 2)
    plt.plot(t, binary_modulated_wave)
    plt.title('Binary Modulation')
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')

    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    main()
